import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"

import Navigation from "./nav"

const Header = ({ siteTitle }) => (
  <div
    style={{
      margin: `2rem auto`,
      maxWidth: 650,
      padding: `0 1rem`
  }}>
    <header
      style={{
        marginBottom: `1.5rem`
      }}>
        <Link to="/"
          style={{
            textShadow: `none`,
            backgroundImage: `none` }}>
            <h3 style={{ display: `inline` }}>
            {siteTitle}
            </h3>
        </Link>
        <Navigation></Navigation>
      </header>
    </div>
  )

  Header.propTypes = {
    siteTitle: PropTypes.string,
  }

  Header.defaultProps = {
    siteTitle: ``,
  }

  export default Header
