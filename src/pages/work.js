import React from "react"
import Layout from "../components/layout"

export default () =>
    (
  <Layout>
  <div>
    <h1>Skills</h1>
    <ul>
      <li>Front-End Development (8/10)</li>
      <li>Back-End Development (8/10)</li>
      <li>Javascript Frameworks (Vue,React,Gatsby)(6/10)</li>
      <li>Responsive/Mobile-Friendly Grids (6/10)</li>
      <li>C# (8/10)</li>
      <li>Asp.net (8/10)</li>
      <li>SQL (7/10)</li>
      <li>NodeJs (6/10)</li>
      <li>Ionic Framework (6/10)</li>
    </ul>

    <h1>My Work</h1>
    <p>Some work samples:
        <ul>
            <li>App Development for <a rel="noopener noreferrer" target="_blank" href="https://www.mijen.tech/">Mijentech</a></li>
            <li>Front and Back-end Development for <a rel="noopener noreferrer" target="_blank" href="https://www.hendrix.edu">Hendrix College</a></li>
            <li>Basic Vue Task App (built using vue-CLI) <a rel="noopener noreferrer" target="_blank" href="https://gitlab.com/tinskanix/mi-day">GitLab repository</a></li>
            <li>First adventures in Dynamic-Binding with KnockoutJS
            <a rel="noopener noreferrer" target="_blank" href="https://gitlab.com/tinskanix/sample-work">Gitlab repository</a></li>
        </ul>
    </p>
  </div>
  </Layout>
)
