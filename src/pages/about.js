import React from "react"

import Layout from "../components/layout"
export default () =>
    (
  <Layout>
  <div>
    <h1>About me</h1>
    <p>
      <div style={{ fontWeight:`bold` }}>I'm a full stack web developer with over 6 years of experience</div>
      I have primarily worked in Higher Ed and supporting local non-profits and grassroot organizations with diverse tech needs. I also helped co-found a tech co-op, 3Fourteen Tech Collective, which is now defunct.
    </p>
  </div>
  </Layout>
)
