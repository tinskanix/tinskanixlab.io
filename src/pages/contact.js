import React from "react"
import Layout from "../components/layout"
export default () =>
    (
  <Layout>
  <div>
    <h1>I'd love to talk! Email me at</h1>
    <p>
      <a href="mailto:mel.beltran@fastmail.com">mel.beltran@fastmail.com</a>
    </p>
  </div>
  </Layout>
)
