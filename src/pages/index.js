import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h2>Full-stack web developer based in Little Rock, AR</h2>
  </Layout>
)

export default IndexPage
